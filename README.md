# ad-plant-api

## 🛠 Developement setup 🛠

To launch a local PostgreSQL/PgAdmin setup:

`docker-compose up`

You can then access PgAdmin via http://localhost:5050/. Credentials:
- User: `pgadmin4@pgadmin.org`
- Password: `admin`

You then have to **add the local PostgreSQL** server on **PgAdmin**:

* **Host name/address** `postgres`
* **Port** `5432`
* **Username** as: `postgres`
* **Password** as: `mysecretpassword`